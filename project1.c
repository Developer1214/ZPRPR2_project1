#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <regex.h>

#define ID 12
#define SUBJECT 152
#define NAME 302
#define DATE 14
#define MAXSIZE 302
#define ATR 4


bool check_pattern(const char *str, const char *pattern) {
    regex_t regex;
    if (regcomp(&regex, pattern, REG_EXTENDED) != 0) {
        return false;
    }
    
    if(regexec(&regex, str, 0, NULL, 0) == 0){
        regfree(&regex);
        return true;
    }
    printf("No pattern str %s pattern %s\n",str,pattern);
    regfree(&regex);
    return false;
}

//Stack overflow function
void clear_stdin() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}


void myAlloc(char ****pP,int * counter){
    (*pP) = (char***)malloc(sizeof(char**)*ATR);
    for(int i = 0;i<ATR;i++){
        (*pP)[i] = (char**)malloc(sizeof(char*)* (*counter));
        for(int j = 0;j<*counter;j++){
            int size;
            switch (i) {
                case 0: size=ID;break;
                case 1: size=SUBJECT;break;
                case 2: size=NAME;break;
                case 3: size=DATE;break;
            }
            (*pP)[i][j] = (char*)malloc(sizeof(char)*size);
        }
    }
}

void mySwitch(char * line, int *i){
    
    char day[3], month[3], year[5], hour[3], minute[3];
    
    switch (*i) {
        
        case 0: printf("ID prispevku: %s\n",line);(*i)++;break;
        case 1: printf("Nazov prispevku: %s\n",line);(*i)++;break;
        case 2: printf("Mena autorov: %s\n",line);(*i)++;break;
        case 3: sscanf(line,"%4s%2s%2s%2s%2s\n",year,month,day,hour,minute); 
                printf("Datum a cas prezentovania: %s.%s.%s %s:%s\n\n",day,month,year,hour,minute);
                *i=0;
                break;
    };
}

void deallocN(char ****pP,int * counter){
    if (*pP == NULL || *counter == 0) {
        return;
    }
    for (int i = 0; i < ATR; i++) {
        if ((*pP)[i] != NULL) {
            for (int j = 0; j < *counter; j++) {
                if ((*pP)[i][j] != NULL) {
                    free((*pP)[i][j]);
                }
            }
            free((*pP)[i]);
        }
    }
    free(*pP);
    *pP = NULL;
}

void open(FILE **f,char ****pP,int * counter){
    if(*f == NULL){
        *f = fopen("KonferencnyZoznam.txt", "r");
        if(*f == NULL){
 
            printf("Neotvoreny subor\n");
            return;
        }
    }  
    if(*pP == NULL){
        char * line = NULL;
        line = (char*)malloc(sizeof(char) * MAXSIZE);
        
        for(int i = 0;feof(*f) == 0;) {
            fgets(line, MAXSIZE, *f);
            if(strlen(line) <= 1){
                continue;
            }
            mySwitch(line, &i);
        }
        free(line);
        line = NULL;
        rewind(*f);
    }else{
        for(int j = 0; j<*counter; j++) {
            for(int i = 0;i<ATR;i++){
                int index = i;
                mySwitch((*pP)[i][j], &index);
            }
        }
    }

}


void add(char ****pP,int * counter){
    (*counter)++;
    if(*pP == NULL){
        myAlloc(pP, counter);
    }
    char * tmp = NULL;
    for(int i = 0;i<ATR;i++){
        int size;
        switch (i) {
            case 0: size=ID;break;
            case 1: size=SUBJECT;break;
            case 2: size=NAME;break;
            case 3: size=DATE;break;
        }
        
        tmp = (char*)malloc(size * sizeof(char));
        fgets(tmp, size, stdin);
        (*pP)[i] = (char**)realloc((*pP)[i], sizeof(char*) * (*counter));
        (*pP)[i][*counter-1] = (char*)malloc(sizeof(char) * size);
        strcpy((*pP)[i][*counter-1],tmp);
        free(tmp);
        tmp = NULL;
        
    }
    printf("Zaznam sa podarilo pridat.\n");
}

void read(FILE **f, char ****pP, int *counter) {
    if (*f == NULL) {
        printf("Neotvoreny subor\n");
        return;
    }
    
    // If already allocated, dealloc deallocate space
    if ((*pP) != NULL) {
        deallocN(pP, counter);
    }

    *counter = 0;

    char *line = (char *)malloc(sizeof(char) * MAXSIZE);
    // Pocet poloziek v subore
    while (fgets(line, MAXSIZE, *f) != NULL) {
        if (strlen(line) == 1 && line[0] == '\n') {
            (*counter)++;
        }
    }
    rewind(*f);

    // Free allocated line
    free(line);


    myAlloc(pP, counter);

    for (int i = 0; i < (*counter); i++) {
        fgets((*pP)[0][i], ID, *f);
        fgets((*pP)[1][i], SUBJECT, *f);
        fgets((*pP)[2][i], NAME, *f);
        fgets((*pP)[3][i], DATE, *f);
        fgetc(*f);
    }
    rewind(*f);

    printf("Nacitanie data\n");
}


void show(char ****pP,int * counter){
    if(*pP == NULL){
        printf("Polia nie su vytvorene.\n");
        return;
    }
    bool find = false;
    char * date = (char*)malloc(9*sizeof(char));
    char * type = (char*)malloc(3*sizeof(char));
    scanf("%s %s",date,type);
    for(int i = 0;i<*counter;i++){
        char * type_pP = (*pP)[0][i];
        char * subj_pP = (*pP)[1][i];
        char * name_pP = (*pP)[2][i];
        char * date_pP = (*pP)[3][i];
        char year[5], month[3], day[3], hour[3], minute[3];
        if (strstr(type_pP,type) != NULL && strstr(date_pP,date) != NULL ) {
            find = true;
            char * tmp = NULL;
            sscanf(date_pP, "%4s%2s%2s%2s%2s",year,month,day,hour,minute);
            sscanf(name_pP, "%[^#]#",name_pP);
            printf("%s%s  %s\t%s\n", hour,minute,name_pP,subj_pP);
        }
    }
    if (!find) {
        printf("Pre dany vstup neexistuju zaznamy.\n");
    }
    free(date);
    date = NULL;
    free(type);
    type = NULL;
    clear_stdin();
}

void write(char**** pP,char *** mena,int * names,int * counter){
    if(*pP == NULL){
        printf("Polia nie su vytvorene.\n");
        return;
    }
    if (*mena != NULL) {
        for (int i = 0; i < *names; i++) {
            free((*mena)[i]);
        }
        free(*mena);
        *mena = NULL;
    }
    *names = 0;
    *mena = (char**)malloc(sizeof(char*));
    char * tmp = NULL;
    for(int i = 0;i<*counter;i++){
        tmp = malloc(strlen((*pP)[2][i]) + 1);
        strcpy(tmp, (*pP)[2][i]);
        tmp[strlen((*pP)[2][i])] = '\0';
        char * t = strtok(tmp, "#");
        while(t != NULL){
            if(strcmp(t,"A") != 0 && strcmp(t, "K") != 0 && t[0] != '\n'){
                *mena = (char**)realloc(*mena, ++(*names)*sizeof(char*));
                (*mena)[*names - 1] = malloc(strlen(t) + 1);
                (*mena)[*names - 1][strlen(t)] = '\0';
                for (int i = 0; t[i]; i++) {
                    t[i] = toupper(t[i]);
                }
                bool copy = false;      
                for(int k = 0;k<*names;k++){
                    if (strcmp((*mena)[k],t) == 0) {
                        copy = true;
                        break;
                    }
                }
                if (!copy) {
                    strcpy((*mena)[*names-1],t);
                    printf("%s\n",t);    
                }
            }
            t = strtok(NULL, "#");
        }

        free(tmp);
        tmp = NULL;
    }
}

void littleDealloc(char ****pP,int * counter){
    if(*pP == NULL){
        printf("Polia nie su vytvorene.\n");
        return;
    }
    int num = 0;
    char * line = malloc(sizeof(char)*SUBJECT);
    if(!fgets(line, SUBJECT, stdin)){
        free(line);
        line = NULL;
        return;
    }
    for(int i = 0;i < *counter;i++){
        if(strcmp((*pP)[1][i],line) == 0){
            num++;
            for(int j = 0;j<ATR;j++){
                free((*pP)[j][i]);
                (*pP)[j][i] = NULL;
            }
            for (int k = i; k < *counter - 1; k++) {
                for (int j = 0; j < ATR; j++) {
                    (*pP)[j][k] = (*pP)[j][k + 1];
                }
            }
            (*counter)--;
            i--;
        }
    }
    printf("Vymazalo sa : %d zaznamov !\n",num);
    free(line);
    line = NULL;
}

void histogram(char ****pP,int * counter){
    if(*pP == NULL){
        printf("Polia nie su vytvorene.\n");
        return;
    }
    int ** intervals = malloc(sizeof(int*)*6);
    for(int i = 0;i<6;i++){
        intervals[i] = (int*)malloc(sizeof(int)*4);
        memset(intervals[i], 0, sizeof(int) * 4);

    }
    for(int i = 0;i<*counter;i++){
        int hour = 0;
        sscanf((*pP)[3][i], "%*8c%2d",&hour);
        int interval = (hour-8)/2;
        if(interval < 0){
            continue;
        }
        
        char * str = malloc(3);
        str[2] = '\0';
        str[0] = (*pP)[0][i][0];
        str[1] = (*pP)[0][i][1];
        if(strcmp(str,"UP") == 0){intervals[interval][0]++;}
        else if(strcmp(str,"UD") == 0){intervals[interval][1]++;}
        else if(strcmp(str,"PP") == 0){intervals[interval][2]++;}
        else if(strcmp(str,"PD") == 0){intervals[interval][3]++;}
        free(str);
        str = NULL;
    }

    printf(" hodina\t\tUP\tUD\tPP\tPD\n");
    for(int i = 0;i<6;i++){
        int startH = 8 + i * 2;
        int endH = startH + 1;
        printf("%02d:00-%02d:59 :\t%d\t%d\t%d\t%d\n",startH,endH,
                intervals[i][0],intervals[i][1],intervals[i][2],intervals[i][3]);
    }
    for(int i = 0;i<6;i++){
        free(intervals[i]);
        intervals[i] = NULL;
    }
    free(intervals);
    intervals = NULL;
}

void close(FILE **f,char ****pP,int * counter,char *** mena,int * names){
    if(*mena != NULL){
        for(int i = 0;i<*names;i++){
            free((*mena)[i]);
            (*mena)[i] = NULL;  
        }
        free(*mena);
        *mena = NULL;
    }
    if (*pP != NULL) {
        deallocN(pP, counter);
    }
    if (*f != NULL) {
        fclose(*f);
        *f = NULL;    
    }
    exit(0);
}

int main(){
    char command;
    FILE *f = NULL;
    char *** pP = NULL;
    int counter = 0;
    char ** mena = NULL;
    int names_number = 0;
    while(1){
        scanf("%1c",&command);
        clear_stdin();
        switch (command) {
            case 'v': open(&f,&pP,&counter); break;
            case 'p': add(&pP,&counter); break;
            case 'n': read(&f,&pP,&counter); break;
            case 's': show(&pP,&counter); break;
            case 'w': write(&pP,&mena,&names_number,&counter); break;
            case 'h': histogram(&pP,&counter); break;
            case 'z': littleDealloc(&pP,&counter);break;
            case 'd': deallocN(&pP,&counter); break;
            case 'k': close(&f,&pP,&counter,&mena,&names_number); break;
            default: printf("Neznamy prikaz\n");break;
        }
    }
    return 0;
}
